#include "HelloWorldScene.h"



Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // UIの読み込み
    Layout* layout = dynamic_cast<cocos2d::ui::Layout*>(GUIReader::getInstance()->widgetFromJsonFile("cocostudio_1.json"));
    this->addChild(layout);

    // キャラクターの読み込み
    ArmatureDataManager::getInstance()->addArmatureFileInfo("calquestTaroAnime.ExportJson");
    Armature *armature = Armature::create("calquestTaroAnime");
    armature->getAnimation()->playWithIndex(0);
    armature->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y + 100));
    this->addChild(armature);
    
    // スケールを調整する
    Size size = Director::getInstance()->getWinSize();
    float scale = size.width / kAPP_WIDTH;
    layout->setScale(scale);
    
    // インスタンスの取得
    inputNumberLabel = static_cast<Text*>(Helper::seekWidgetByName(layout, "inputnumber"));
    questionLabel = static_cast<Text*>(Helper::seekWidgetByName(layout, "questionlabel"));

    // ボタンイベントのセット
    for (int i=0; i<13; i++)
    {
		Button *button;
		if (i < 10)
		{
            
            auto btnStr = StringUtils::format("btn%d", i);
			button = static_cast<Button*>(Helper::seekWidgetByName(layout, btnStr.c_str()));
		} else {
            if (i == 10) {
                button = static_cast<Button*>(Helper::seekWidgetByName(layout, "btndot"));
            } else if (i == 11) {
                button = static_cast<Button*>(Helper::seekWidgetByName(layout, "btnclear"));
            } else if (i == 12) {
                button = static_cast<Button*>(Helper::seekWidgetByName(layout, "btnenter"));                
            }
		}
		button->setTag(i);
		button->addTouchEventListener(this, toucheventselector(HelloWorld::touchEvent));
    }

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
//    auto closeItem = MenuItemImage::create(
//                                           "CloseNormal.png",
//                                           "CloseSelected.png",
//                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
//
//	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
//                                origin.y + closeItem->getContentSize().height/2));
//
//    // create menu, it's an autorelease object
//    auto menu = Menu::create(closeItem, NULL);
//    menu->setPosition(Vec2::ZERO);
//    this->addChild(menu, 1);
//
//    /////////////////////////////
//    // 3. add your codes below...
//
//    // add a label shows "Hello World"
//    // create and initialize a label
//
//    auto label = LabelTTF::create("Hello World", "Arial", 24);
//
//    // position the label on the center of the screen
//    label->setPosition(Vec2(origin.x + visibleSize.width/2,
//                            origin.y + visibleSize.height - label->getContentSize().height));
//
//    // add the label as a child to this layer
//    this->addChild(label, 1);
//
//    // add "HelloWorld" splash screen"
//    auto sprite = Sprite::create("HelloWorld.png");
//
//    // position the sprite on the center of the screen
//    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
//
//    // add the sprite as a child to this layer
//    this->addChild(sprite, 0);
    
    return true;
}


void HelloWorld::touchEvent(Button *pSender, TouchEventType type)
{
    switch (type)
    {
        case TOUCH_EVENT_BEGAN:
            break;
        case TOUCH_EVENT_MOVED:
            break;
        case TOUCH_EVENT_ENDED:
        {
        	log("tap=%d", pSender->getTag());


        	string resultString;
        	if (pSender->getTag() == 10)
        	{
        		resultString = getCalcString(string("."));
            } else if (pSender->getTag() == 11) {
                resultString = getCalcString(string("-"));
            } else if (pSender->getTag() == 12) {
                resultString = getCalcString(string("="));
        	} else {
        		resultString = getCalcString(StringUtils::format("%d", pSender->getTag()));
        	}
        	inputNumberLabel->setString(resultString);
            break;
        }
        case TOUCH_EVENT_CANCELED:
            break;
        default:
            break;
    }
}

string HelloWorld::getCalcString(string inputString)
{
	string currentString = inputNumberLabel->getString();
	if (currentString == "0") {
		currentString.resize(0);
	}

    if (inputString == "-") {
        // 0に戻す
        currentString.resize(0);
        
//        // 一文字削除
//        if (!currentString.empty()) {
//            currentString = currentString.substr(0, currentString.size()-1);
//        }
    } else if (inputString == "=") {
        currentString = string("0");
    } else if (inputString == ".") {
		bool includedDot = false;
		for (int i=0; i<currentString.size(); i++) {
			if (currentString.at(i) == '.') {
				includedDot = true;
			}
		}
		if (!includedDot) {
			currentString += inputString;
		}
	} else {
		currentString += inputString;
	}

	if (currentString == "")
	{
		return string("0");
	} else {
		return currentString;
	}
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
