#include "AppDelegate.h"
#include "HelloWorldScene.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("My Game");
        director->setOpenGLView(glview);
    }


//    glview->setDesignResolutionSize(640.0/scale, 960.0/scale, ResolutionPolicy::SHOW_ALL);
//    director->setContentScaleFactor(2.0f);
    
    
    /*
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

//    folderPaths.push_back("iOS-folder");

    //画面のスクリーンサイズを取得
    CCEGLView* view = director->ge
    		CCDirector::sharedDirector()->getOpenGLView();
    CCSize frame = view->getFrameSize();

    //サイズの大きさでRetina(4-inch)の判定をしている。

    if (frame.width > 480) {

        pDirector->setContentScaleFactor(2.f);


        if (frame.width==1136.0) {

//            folderPaths2.push_back("Retina4");

            CCEGLView::sharedOpenGLView()->setDesignResolutionSize(568, 320, kResolutionExactFit);

        } else {

//            folderPaths2.push_back("Retina3.5");

            CCEGLView::sharedOpenGLView()->setDesignResolutionSize(480, 320, kResolutionExactFit);
        }


    }else{

        folderPaths2.push_back("normal");
        CCEGLView::sharedOpenGLView()->setDesignResolutionSize(480, 320, kResolutionExactFit);

    }


    #else
    // iOS以外のデバイスの場合
//    folderPaths.push_back("iOS-folder");
//    folderPaths2.push_back("normal");

    CCEGLView::sharedOpenGLView()->setDesignResolutionSize(480, 320, kResolutionExactFit);
#endif
*/

    // turn on display FPS
//    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    auto scene = HelloWorld::createScene();
    
    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
